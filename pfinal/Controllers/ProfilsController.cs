﻿using pfinal.Manager;
using pfinal.Models;
using pfinal.Models.Form;
using pfinal.Models.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace pfinal.Controllers
{
    [RoutePrefix("api/Profil")]
    public class ProfilsController : ApiController
    {

        [HttpPut]
        [Route("Verified")]
        [Authorize(Roles = "Admin")]
        public IHttpActionResult Verified(VerifiedForm vf)
        {         
            bool ver = ProfilsManager.Verified(vf.AppId);
            if (ver)
                return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = ver });
            else
                return Ok(new Result { HttpCode = HttpStatusCode.BadRequest, Obj = ver });                
        }

        [HttpGet]
        [Route("Get/{id}")]
        public IHttpActionResult Get(String id)
        {
            return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = ProfilsManager.GetProfil(id) });
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("Notverified")]
        public IHttpActionResult GetAllNotVerified()
        {
            return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = ProfilsManager.GetAllNotVerified() });
        }


        [HttpGet]
        [Route("Gaa/{id}")]
        public IHttpActionResult GetByArrondissementId(int id)
        {
            return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = ProfilsManager.GetAllProfilByArrondisementId(id) });
        }

        [HttpGet]
        [Route("Get")]
        public IHttpActionResult GetAll()
        {
            return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = ProfilsManager.GetAll() });
        }

        [HttpPut]
        [Route("Modify")]
        public IHttpActionResult Edit(EditProfilMain emf)
        {
                if (!(String.IsNullOrEmpty(emf.Day) && String.IsNullOrEmpty(emf.Year) && String.IsNullOrEmpty(emf.Year)))
                {
                    if (emf.Day.IsInt() && emf.Month.IsInt() && emf.Year.IsInt())
                    {
                        if (int.Parse(emf.Day) < 1 || int.Parse(emf.Day) > 31)
                            ModelState.AddModelError("Day", "Day must be between 1 and 31");
                        if (int.Parse(emf.Month) < 1 || int.Parse(emf.Month) > 12)
                            ModelState.AddModelError("Month", "Month must be between 1 and 12");
                        if (int.Parse(emf.Year) < 1900 || int.Parse(emf.Year) > DateTime.Now.Year)
                            ModelState.AddModelError("Year", "Year must be between 1900 and " + DateTime.Now.Year);

                    }
                    else
                    {
                        if (!emf.Day.IsInt()) ModelState.AddModelError("Day", "Day must be a number");
                        if (!emf.Year.IsInt()) ModelState.AddModelError("Year", "Year must be a number");
                        if (!emf.Month.IsInt()) ModelState.AddModelError("Month", "Month Must be a number");
                    }


                }

                if (!ModelState.IsValid)
                    return Ok(new Result { HttpCode = HttpStatusCode.BadRequest, Obj = ModelState.getListErrorAndKey(null, typeof(EditProfilMain)) });

                Profil profil = ProfilsManager.GetProfil(User.Identity.GetUserId());

                if (profil != null)
                {

                    if (emf.Firstname != null && emf.Firstname != "")
                        profil.Firstname = emf.Firstname;
                    if (emf.Lastname != null && emf.Lastname != "")
                        profil.Lastname = emf.Lastname;
                    if (emf.Gender != null && emf.Gender != "")
                        profil.Gender = emf.Gender;
                    if (emf.Phone != null && emf.Phone != "")
                        profil.Phone = emf.Phone;
                    if ((!String.IsNullOrEmpty(emf.Day)) && (!String.IsNullOrEmpty(emf.Month)) && (!String.IsNullOrEmpty(emf.Year)))
                        profil.Birthday = new DateTime(int.Parse(emf.Year), int.Parse(emf.Month), int.Parse(emf.Day));


                    Profil p = ProfilsManager.Edit(profil);

                    return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = p });
                }
                else
                    return Ok(new Result { HttpCode = HttpStatusCode.BadRequest, Obj = new Error { Key = null, Msg = "Profil is invalid" } });
        }
    }
}
