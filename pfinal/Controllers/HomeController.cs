﻿using pfinal.Manager;
using pfinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pfinal.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            List<Arrondisement> list = AdressesManager.GetAllArrondissement();

            ViewBag.arrondisement = list;
            return View();
        }
    }
}
