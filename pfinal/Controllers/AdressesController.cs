﻿using pfinal.Manager;
using pfinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using pfinal.Models.Form;
using pfinal.Models.NotMapped;

namespace pfinal.Controllers
{
    [RoutePrefix("api/Adresse")]
    public class AdressesController : ApiController
    {
        [HttpGet]
        [Route("Getallar")]
        public IHttpActionResult GetAllArrondisement()
        {
            return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = AdressesManager.GetAllArrondissement() });
        }

        [Authorize]
        [HttpPut]
        [Route("Modify")]
        public IHttpActionResult Edit(EditAdressesForm adresse)
        {
            Profil profil = ProfilsManager.GetProfil(User.Identity.GetUserId());

            if(adresse.CodePostal.Length == 6)
            {
                if (adresse.CodePostal.Substring(0, 3).ToLower() != AdressesManager.GetArrondisementById(adresse.ArrondisementId).Code.ToLower())
                    ModelState.AddModelError("CodePostal", "Your code postal do not fit with this arrondissement");
            }

            if (!ModelState.IsValid)
                return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = ModelState.getListErrorAndKey(null, typeof(EditAdressesForm)) });

            Adresse adr = AdressesManager.Edit(new Adresse
            {
                Id = (int) profil.AdresseId,
                ArrondisementId = adresse.ArrondisementId,
                CivicNumber = adresse.CivicNumber,
                CodePostal = adresse.CodePostal,
                StreetName = adresse.StreetName
            });

            return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = adr });
        }
    }
}
