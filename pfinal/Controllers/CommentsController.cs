﻿using pfinal.Manager;
using pfinal.Models;
using pfinal.Models.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using pfinal.Models.NotMapped;

namespace pfinal.Controllers
{
    [RoutePrefix("api/Comment")]
    public class CommentsController : ApiController
    {
        [HttpGet]
        [Route("Geta/{arrId}")]
        public IHttpActionResult GetALlByArrondissementId(int arrId)
        {
            return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = CommentsManager.GetAllsCommentByArrondissement(arrId) });
        }

        [Route("Get/{appId}")]
        public IHttpActionResult GetALlByAppUserid(String appId)
        {
            return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = CommentsManager.GetCommentsByApplicationUserId(appId) });
        }

        [Authorize]
        [HttpPost]
        [Route("Add")]
        public IHttpActionResult Add(NewCommentForm nfc)
        {
            if (!ModelState.IsValid)
                return Ok(new Result { HttpCode = HttpStatusCode.BadRequest, Obj = ModelState.getListErrorAndKey(null, typeof(NewCommentForm)) });

            Comment c = CommentsManager.Add(new Comment
            {
                Message = nfc.Message,
                ApplicationUserId = User.Identity.GetUserId(),
                DatePublication = DateTime.Now,
            });

            return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = c });
        }

        [Authorize]
        [HttpDelete]
        [Route("Delete /{id}")]
        public IHttpActionResult Delete(int id)
        {
            return Ok();
        }
        
    }
}
