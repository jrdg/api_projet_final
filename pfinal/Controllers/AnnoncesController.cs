﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using pfinal.Models;
using pfinal.Models.Form;
using pfinal.Models.NotMapped;
using pfinal.Manager;
using Microsoft.AspNet.Identity;

namespace pfinal.Controllers
{
    [RoutePrefix("api/Annonce")]
    public class AnnoncesController : ApiController
    {
        [HttpGet]
        [Route("Getappid/{appid}")]
        public IHttpActionResult GetAllAnnonceByApplicationUserId(String appid)
        {
            return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = AnnoncesManager.GetAnnonceByUserId(appid) });
        }

        [HttpGet]
        [Route("Gettype")]
        public IHttpActionResult GetAllType()
        {
            return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = AnnoncesManager.GetAllType() });
        }

        [Authorize]
        [HttpPost]
        [Route("Search")]
        public IHttpActionResult Search(SearchAnnonceResult sar)
        {
            sar.ArrondissementId = ProfilsManager.GetProfil(User.Identity.GetUserId()).Adresse.ArrondisementId;
            return Ok(AnnoncesManager.Search(sar));
        }

        [HttpGet]
        [Route("Gaa/{id}")]
        public IHttpActionResult GetAllByArrondissementid(int id)
        {
            return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = AnnoncesManager.GetAllByArrondissementId(id) });
        }

        [Authorize]
        [HttpPut]
        [Route("Modify")]
        public IHttpActionResult EditAnnonce(EditAnnonceForm model)
        {
            if (!AnnoncesManager.TypeAnnonceValid(model.TypeAnnonceId))
                ModelState.AddModelError("TypeAnnonceId", "The type is invalid");

            if (!this.ModelState.IsValid)
                return Ok(new Result { HttpCode = HttpStatusCode.BadRequest, Obj = ModelState.getListErrorAndKey(null, typeof(EditAnnonceForm)) });

            Annonce annonce = AnnoncesManager.Get(model.AnnonceId);

            if (annonce != null)
            {
                if(annonce.ApplicationUserId == User.Identity.GetUserId())
                {
                    annonce.Title = model.Title;
                    annonce.Description = model.Description;
                    annonce.TypeAnnonceId = model.TypeAnnonceId;
                   

                    Annonce a = AnnoncesManager.Edit(annonce);

                    if (a != null)
                        return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = a });
                }
            }

            return Ok(new Result { HttpCode = HttpStatusCode.BadRequest , Obj = new Error { Key = null , Msg = "Annonce is invalid" } });
        }

        [Authorize]
        [HttpDelete]
        [Route("Delete/{id}")]
        public IHttpActionResult DeleteAnnonce(int id)
        {
            Annonce annonce = AnnoncesManager.Get(id);

            if(annonce != null)
            {
                if(annonce.ApplicationUserId == User.Identity.GetUserId() || User.IsInRole("Admin"))
                {
                    if (AnnoncesManager.Delete(id))
                        return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = new Error { Key = null, Msg = "annonce have been delete" } });
                }
            }

            return Ok(new Result { HttpCode = HttpStatusCode.BadRequest, Obj = new Error { Key = null , Msg = "Invalid annonce" } });
        }

        [Authorize]
        [HttpPost]
        [Route("Add")]
        public IHttpActionResult AddAnnonce(NewAnnonceForm model)
        {
            if (!AnnoncesManager.TypeAnnonceValid(model.TypeAnnonceId))
                ModelState.AddModelError("TypeAnnonceId", "The type is invalid");

            if (!ModelState.IsValid)
                return Ok(new Result { HttpCode = HttpStatusCode.BadRequest, Obj = ModelState.getListErrorAndKey(null, typeof(NewAnnonceForm)) });

            Annonce annonce = AnnoncesManager.Add(new Annonce
            {
                Title = model.Title,
                Description = model.Description,
                ApplicationUserId = User.Identity.GetUserId(),
                DatePublication = DateTime.Now,
                TypeAnnonceId = model.TypeAnnonceId,
                ArrondisementId = ProfilsManager.GetProfil(User.Identity.GetUserId()).Adresse.ArrondisementId     
            });

            return Ok(new Result { HttpCode = HttpStatusCode.OK , Obj = annonce });
        }

        
        [HttpGet]
        [Route("Get")]
        public IHttpActionResult GetAll()
        {
            return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = AnnoncesManager.GetAll() });
        }
        

        [HttpGet]
        [Route("Get/{id}")]
        public IHttpActionResult GetAnnonce(int id)
        {
            Annonce annonce = AnnoncesManager.Get(id);

            if (annonce != null)
                return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = annonce });
            else
                return Ok(new Result { HttpCode = HttpStatusCode.BadRequest, Obj = new Error { Key = null, Msg = "Invalid annonce" } });
        }
    }
}