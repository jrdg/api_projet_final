﻿using pfinal.Models.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using pfinal.Manager;
using pfinal.Models;
using pfinal.Models.NotMapped;
using Microsoft.AspNet.Identity;
using System.IO;
using System.Drawing;

namespace pfinal.Controllers
{
    [RoutePrefix("api/Image")]
    public class ImagesController : ApiController
    {

        [Route("Android")]
        [HttpPost]
        [Authorize]
        public IHttpActionResult AddAndroid(AndroidAddImage aai)
        {
            bool valid = false;

            int index = aai.B64.IndexOf(',');

            //cette ligne es a utiliser pour tester seulement avec js on fais un substring avec lindex get pls haut pour seulement aller chercher le base64 string
            // String good = aai.B64.Substring(index+1);

            String good = aai.B64;

            var bytes = Convert.FromBase64String(good);

            String ext = null;

            if (good.Substring(0, 1) == "i")
                ext = "png";
            else if (good.Substring(0, 1) == "/")
                ext = "jpg";
            else
                ModelState.AddModelError("B64", "We only accept jpg and png");

            if (bytes.Length > 3000000)
                ModelState.AddModelError("B64", "File is too big");

            if (!ModelState.IsValid)
                return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = ModelState.getListErrorAndKey(null, typeof(AndroidAddImage)) });

            Models.Image img = ImagesManager.Add(new Models.Image
            {
                Name = "androidimg." + ext,
                ContentType = ext == "jpg" ? "image/jpeg" : "image/png"
            });


            using (var imageFile = new FileStream(FilesManager.FilesPath+"-img=" + img.Id + "_=" + "androidimg" +"."+ext, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }

            if (aai.BenefType == "Profil")
            {
                Profil p = ProfilsManager.GetProfil(User.Identity.GetUserId());
                p.ImageId = img.Id;
                ProfilsManager.Edit(p);
                valid = true;
            }
            else if (aai.BenefType == "Annonce")
            {

                if (aai.AnnonceId != null)
                {
                    if (aai.AnnonceId.IsInt())
                    {
                        Annonce a = AnnoncesManager.Get(int.Parse(aai.AnnonceId));

                        if (a != null)
                        {
                            a.ImageId = img.Id;
                            AnnoncesManager.Edit(a);
                            valid = true;
                        }
                    }
                }
            }
            else if (aai.BenefType == "Preuve")
            {
                if (ProfilsManager.GetProfil(User.Identity.GetUserId()).IsVerified == false)
                {
                    Preuve p = PreuvesManager.GetPreuveByAppId(User.Identity.GetUserId());

                    if (p != null)
                    {
                        Profil prof = ProfilsManager.GetProfil(User.Identity.GetUserId());
                        prof.PreuveId = null;
                        ProfilsManager.Edit(prof);
                        PreuvesManager.Delete(p.Id);
                    }


                    Preuve pr = PreuvesManager.Add(new Preuve
                    {
                        ApplicationUserId = User.Identity.GetUserId(),
                        ImageId = img.Id,
                        IsVerified = false
                    });

                    Profil pro = ProfilsManager.GetProfil(User.Identity.GetUserId());
                    pro.PreuveId = pr.Id;
                    ProfilsManager.Edit(pro);
                    valid = true;
                }
                else
                    ModelState.AddModelError("B64", "Your account is already verified");
            }

            String path = "http://teamapi.azurewebsites.net/Content/image/" + "-img=" + img.Id + "_=" + "androidimg" + "." + ext;

            if (valid)
                return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = new { Image = img, Url = path } });
            else
                return Ok(new Result { HttpCode = HttpStatusCode.BadRequest, Obj = ModelState.getListErrorAndKey(null, typeof(AndroidAddImage)) });
        }

        [Route("Add")]
        [HttpPost]
        [Authorize]
        public IHttpActionResult Add()
        {
            var files = HttpContext.Current.Request.Files;
            String errMsg = null;

            if (files.Count > 0)
            {
                var file = files[0];             
                String[] contentTypeAllowed = { "image/jpeg", "image/png" };

                if (!(file.ContentLength > 3000000))
                {
                    if (contentTypeAllowed.Contains(file.ContentType))
                    {
                        Models.Image img = ImagesManager.Add(new Models.Image
                        {
                            ContentType = file.ContentType,
                            Name = file.FileName,
                        });

                        String path = "http://teamapi.azurewebsites.net/Content/image/"+FilesManager.UploadFile(file, img.Id);

                        
                        if(HttpContext.Current.Request.Form["BenefType"] == "Profil")
                        {
                            Profil p = ProfilsManager.GetProfil(User.Identity.GetUserId());
                            p.ImageId = (int?)img.Id;
                            ProfilsManager.Edit(p);
                        }
                        else if(HttpContext.Current.Request.Form["BenefType"] == "Annonce")
                        {
                            String aid = HttpContext.Current.Request.Form["AnnonceId"];

                            if (aid != null)
                            {
                                if(aid.IsInt())
                                {
                                    Annonce a = AnnoncesManager.Get(int.Parse(HttpContext.Current.Request.Form["AnnonceId"]));

                                    if(a != null)
                                    {
                                        a.ImageId = img.Id;
                                        AnnoncesManager.Edit(a);   
                                    }
                                }
                            }
                        }
                        else if (HttpContext.Current.Request.Form["BenefType"] == "Preuve")
                        {
                            if (ProfilsManager.GetProfil(User.Identity.GetUserId()).IsVerified == false)
                            {
                                Preuve p = PreuvesManager.GetPreuveByAppId(User.Identity.GetUserId());

                                if (p != null)
                                {
                                    Profil prof = ProfilsManager.GetProfil(User.Identity.GetUserId());
                                    prof.PreuveId = null;
                                    ProfilsManager.Edit(prof);
                                    PreuvesManager.Delete(p.Id);
                                }


                                Preuve pr = PreuvesManager.Add(new Preuve
                                {
                                    ApplicationUserId = User.Identity.GetUserId(),
                                    ImageId = img.Id,
                                    IsVerified = false
                                });

                                Profil pro = ProfilsManager.GetProfil(User.Identity.GetUserId());
                                pro.PreuveId = pr.Id;
                                ProfilsManager.Edit(pro);
                            }
                            else
                                errMsg = "Profil is already verified";                        
                        }

                        return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = new { Image = img , Url = path } });
                    }
                    else
                        errMsg = file.ContentType + " is not allowed we only accept jpg and png";
                }
                else
                    errMsg = "File is too big";

            }
            else
                errMsg = "Please select a jpg or png";

            return Ok(new Result { HttpCode = HttpStatusCode.OK, Obj = new Error { Key = null, Msg = errMsg } });

        }
    }
}
