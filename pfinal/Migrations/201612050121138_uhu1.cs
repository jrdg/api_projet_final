namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class uhu1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "Profil_Id", "dbo.Profils");
            DropIndex("dbo.Comments", new[] { "Profil_Id" });
            DropColumn("dbo.Comments", "Profil_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comments", "Profil_Id", c => c.Int());
            CreateIndex("dbo.Comments", "Profil_Id");
            AddForeignKey("dbo.Comments", "Profil_Id", "dbo.Profils", "Id");
        }
    }
}
