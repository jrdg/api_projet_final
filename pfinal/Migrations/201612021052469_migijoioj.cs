namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migijoioj : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Adresses", "ArrondisementId", "dbo.Arrondisements");
            DropIndex("dbo.Adresses", new[] { "ArrondisementId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Adresses", "ArrondisementId");
            AddForeignKey("dbo.Adresses", "ArrondisementId", "dbo.Arrondisements", "Id", cascadeDelete: true);
        }
    }
}
