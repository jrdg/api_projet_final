namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ouhih : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Profils", "Email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Profils", "Email");
        }
    }
}
