namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class idgwydg : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Preuves",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ImageId = c.Int(nullable: false),
                        ApplicationUserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId)
                .ForeignKey("dbo.Images", t => t.ImageId, cascadeDelete: true)
                .Index(t => t.ImageId)
                .Index(t => t.ApplicationUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Preuves", "ImageId", "dbo.Images");
            DropForeignKey("dbo.Preuves", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Preuves", new[] { "ApplicationUserId" });
            DropIndex("dbo.Preuves", new[] { "ImageId" });
            DropTable("dbo.Preuves");
        }
    }
}
