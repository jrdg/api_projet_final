namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class uhu : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Profils", "ApplicationUserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Profils", "ApplicationUserId");
            AddForeignKey("dbo.Profils", "ApplicationUserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Profils", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Profils", new[] { "ApplicationUserId" });
            AlterColumn("dbo.Profils", "ApplicationUserId", c => c.String());
        }
    }
}
