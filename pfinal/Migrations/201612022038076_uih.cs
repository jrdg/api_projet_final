namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class uih : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Profils", "AdresseId");
            AddForeignKey("dbo.Profils", "AdresseId", "dbo.Adresses", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Profils", "AdresseId", "dbo.Adresses");
            DropIndex("dbo.Profils", new[] { "AdresseId" });
        }
    }
}
