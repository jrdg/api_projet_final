namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class uigigh : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "ProfilId", "dbo.Profils");
            DropIndex("dbo.Comments", new[] { "ProfilId" });
            RenameColumn(table: "dbo.Comments", name: "ProfilId", newName: "Profil_Id");
            AddColumn("dbo.Comments", "ApplicationUserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Comments", "Profil_Id", c => c.Int());
            CreateIndex("dbo.Comments", "ApplicationUserId");
            CreateIndex("dbo.Comments", "Profil_Id");
            AddForeignKey("dbo.Comments", "ApplicationUserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Comments", "Profil_Id", "dbo.Profils", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "Profil_Id", "dbo.Profils");
            DropForeignKey("dbo.Comments", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Comments", new[] { "Profil_Id" });
            DropIndex("dbo.Comments", new[] { "ApplicationUserId" });
            AlterColumn("dbo.Comments", "Profil_Id", c => c.Int(nullable: false));
            DropColumn("dbo.Comments", "ApplicationUserId");
            RenameColumn(table: "dbo.Comments", name: "Profil_Id", newName: "ProfilId");
            CreateIndex("dbo.Comments", "ProfilId");
            AddForeignKey("dbo.Comments", "ProfilId", "dbo.Profils", "Id", cascadeDelete: true);
        }
    }
}
