namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sijdbidig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Annonces", "ImageId", c => c.Int());
            CreateIndex("dbo.Annonces", "ImageId");
            AddForeignKey("dbo.Annonces", "ImageId", "dbo.Images", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Annonces", "ImageId", "dbo.Images");
            DropIndex("dbo.Annonces", new[] { "ImageId" });
            DropColumn("dbo.Annonces", "ImageId");
        }
    }
}
