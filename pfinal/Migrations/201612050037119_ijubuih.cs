namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ijubuih : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        DatePublication = c.DateTime(nullable: false),
                        ProfilId = c.Int(nullable: false),
                        AnnonceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Annonces", t => t.AnnonceId, cascadeDelete: true)
                .ForeignKey("dbo.Profils", t => t.ProfilId, cascadeDelete: true)
                .Index(t => t.ProfilId)
                .Index(t => t.AnnonceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "ProfilId", "dbo.Profils");
            DropForeignKey("dbo.Comments", "AnnonceId", "dbo.Annonces");
            DropIndex("dbo.Comments", new[] { "AnnonceId" });
            DropIndex("dbo.Comments", new[] { "ProfilId" });
            DropTable("dbo.Comments");
        }
    }
}
