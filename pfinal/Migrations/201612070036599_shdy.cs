namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class shdy : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Profils", "ImageId", c => c.Int());
            CreateIndex("dbo.Profils", "ImageId");
            AddForeignKey("dbo.Profils", "ImageId", "dbo.Images", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Profils", "ImageId", "dbo.Images");
            DropIndex("dbo.Profils", new[] { "ImageId" });
            DropColumn("dbo.Profils", "ImageId");
        }
    }
}
