namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class iuiuhihi : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Adresses", "ArrondisementId");
            AddForeignKey("dbo.Adresses", "ArrondisementId", "dbo.Arrondisements", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Adresses", "ArrondisementId", "dbo.Arrondisements");
            DropIndex("dbo.Adresses", new[] { "ArrondisementId" });
        }
    }
}
