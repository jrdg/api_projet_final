namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class msihdh : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Annonces", "ArrondisementId", c => c.Int(nullable: false));
            CreateIndex("dbo.Annonces", "ArrondisementId");
            AddForeignKey("dbo.Annonces", "ArrondisementId", "dbo.Arrondisements", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Annonces", "ArrondisementId", "dbo.Arrondisements");
            DropIndex("dbo.Annonces", new[] { "ArrondisementId" });
            DropColumn("dbo.Annonces", "ArrondisementId");
        }
    }
}
