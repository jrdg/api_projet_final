namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class jsjdiidg : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Profils", new[] { "ApplicationUserId" });
            DropPrimaryKey("dbo.Profils");
            AlterColumn("dbo.Profils", "ApplicationUserId", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Profils", "ApplicationUserId");
            CreateIndex("dbo.Profils", "ApplicationUserId");
            DropColumn("dbo.Profils", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Profils", "Id", c => c.Int(nullable: false, identity: true));
            DropIndex("dbo.Profils", new[] { "ApplicationUserId" });
            DropPrimaryKey("dbo.Profils");
            AlterColumn("dbo.Profils", "ApplicationUserId", c => c.String(maxLength: 128));
            AddPrimaryKey("dbo.Profils", "Id");
            CreateIndex("dbo.Profils", "ApplicationUserId");
        }
    }
}
