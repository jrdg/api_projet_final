namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class wydgwyge : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Preuves", "ImageId", "dbo.Images");
            DropIndex("dbo.Preuves", new[] { "ImageId" });
            AlterColumn("dbo.Preuves", "ImageId", c => c.Int(nullable: false));
            CreateIndex("dbo.Preuves", "ImageId");
            AddForeignKey("dbo.Preuves", "ImageId", "dbo.Images", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Preuves", "ImageId", "dbo.Images");
            DropIndex("dbo.Preuves", new[] { "ImageId" });
            AlterColumn("dbo.Preuves", "ImageId", c => c.Int());
            CreateIndex("dbo.Preuves", "ImageId");
            AddForeignKey("dbo.Preuves", "ImageId", "dbo.Images", "Id");
        }
    }
}
