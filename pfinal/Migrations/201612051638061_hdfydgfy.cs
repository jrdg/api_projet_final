namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class hdfydgfy : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "AnnonceId", "dbo.Annonces");
            DropIndex("dbo.Comments", new[] { "AnnonceId" });
            DropColumn("dbo.Comments", "AnnonceId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comments", "AnnonceId", c => c.Int(nullable: false));
            CreateIndex("dbo.Comments", "AnnonceId");
            AddForeignKey("dbo.Comments", "AnnonceId", "dbo.Annonces", "Id", cascadeDelete: true);
        }
    }
}
