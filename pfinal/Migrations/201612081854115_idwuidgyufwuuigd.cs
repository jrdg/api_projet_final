namespace pfinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class idwuidgyufwuuigd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Profils", "PreuveId", c => c.Int());
            AddColumn("dbo.Preuves", "IsVerified", c => c.Boolean(nullable: false));
            CreateIndex("dbo.Profils", "PreuveId");
            AddForeignKey("dbo.Profils", "PreuveId", "dbo.Preuves", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Profils", "PreuveId", "dbo.Preuves");
            DropIndex("dbo.Profils", new[] { "PreuveId" });
            DropColumn("dbo.Preuves", "IsVerified");
            DropColumn("dbo.Profils", "PreuveId");
        }
    }
}
