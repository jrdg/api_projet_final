﻿using pfinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace pfinal.Manager
{
    public static class UsersManager
    {
        public static bool DoEmailExist(String email)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                ApplicationUser user = ctx.Users.FirstOrDefault(u => u.Email == email);

                if (user != null)
                    return true;
                else
                    return false;
            }
        }

        public static ApplicationUser getApplicationUserByMail(String email)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                ApplicationUser user = ctx.Users.FirstOrDefault(u => u.Email == email);

                if (user != null)
                    return user;
                else
                    return null;
            }
        }

        public static ApplicationUser getApplicationUserById(String id)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                ApplicationUser user = ctx.Users.FirstOrDefault(u => u.Id == id);

                if (user != null)
                    return user;
                else
                    return null;
            }
        }     
    }
}