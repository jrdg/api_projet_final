﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace pfinal.Manager
{
    public class FilesManager
    {
        public static char DirSeparator = System.IO.Path.DirectorySeparatorChar;
        public static String FilesPath = HttpContext.Current.Server.MapPath("~\\Content" + DirSeparator + "image" + DirSeparator);

        public static String UploadFile(HttpPostedFile file, int id)
        {
            // on regarde si le directory existe
            if (!Directory.Exists(FilesPath))
            {
                // si il nexiste pas on le cree
                Directory.CreateDirectory(FilesPath);
            }

            // Set our full path for savings
            String path = FilesPath + DirSeparator + "-img="+id+ "_=" +file.FileName;

            // Save our file
            file.SaveAs(Path.GetFullPath(path));

            return "-img=" + id + "_=" + file.FileName;
        }

        //regarde si le fichier existe deja
        public static bool doExist(String name)
        {
            String path = FilesPath + DirSeparator + name;
            return File.Exists(Path.GetFullPath(path));
        }

        public static void Delete(String name)
        {
            String path = FilesPath + DirSeparator + name;
            File.Delete(Path.GetFullPath(path));
        }

        public static void Rename(String oldfile, String newfile)
        {
            String nf = FilesPath + DirSeparator + newfile;
            String of = FilesPath + DirSeparator + oldfile;

            File.Copy(Path.GetFullPath(of), Path.GetFullPath(nf));
            File.Delete(Path.GetFullPath(of));
        }
    }
}