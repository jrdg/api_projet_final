﻿using pfinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Http;

namespace pfinal.Manager
{
    public class ProfilsManager
    {

        public static bool Verified(String appid)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Profil p = ctx.Profils.FirstOrDefault(pro => pro.ApplicationUserId == appid);

                if(p!= null)
                {
                    p.IsVerified = true;
                    ctx.SaveChanges();
                    return true;
                }

                return false;
            }
        }

        public static Profil Add(Profil profil)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Profil p = ctx.Profils.Add(profil);
                ctx.SaveChanges();
                return p;
            }
        }
        

        public static List<Profil> GetAllNotVerified()
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                return ctx.Profils.Include("Adresse.Arrondisement").AsNoTracking().Include("Image").Include("Preuve.Image").Where(p => (p.IsVerified == false) && (p.PreuveId != null)).ToList();
            }
        }

        public static List<Profil> GetAllProfilByArrondisementId(int id)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                return ctx.Profils.Include(mbox => mbox.Adresse.Arrondisement).AsNoTracking().Include("Image").Include("Preuve.Image").Where(p => p.Adresse.ArrondisementId == id).ToList();
            }
        }

        /*
        public static void Delete(int id)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Profil profil = ctx.Profils.FirstOrDefault(p => p.Id == id);

                if(profil != null)
                {
                    ctx.Profils.Remove(profil);
                    ctx.SaveChanges();
                }
            }
        }
        */

        public static Profil Edit(Profil profil)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Profil r = ctx.Profils.FirstOrDefault(a => a.ApplicationUserId == profil.ApplicationUserId);

                if (r != null)
                {
                    r.ImageId = profil.ImageId;
                    r.AdresseId = profil.AdresseId;
                    r.Birthday = profil.Birthday;
                    r.Firstname = profil.Firstname;
                    r.Lastname = profil.Lastname;
                    r.Email = profil.Email;
                    r.Gender = profil.Gender;
                    r.Phone = profil.Phone;
                    r.IsVerified = profil.IsVerified;
                    r.PreuveId = profil.PreuveId;
                    ctx.SaveChanges();
                    return r;
                }

                return null;
            }
        }

        public static Profil GetProfil(String id)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Profil profil = ctx.Profils.Include("Adresse.Arrondisement").AsNoTracking().Include("Image").Include("Preuve.Image").FirstOrDefault(u => (u.ApplicationUserId == id) || (u.Email == id));

                if (profil != null)
                    return profil;
                else
                    return null;
            }
        }

        public static List<Profil> GetAll()
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                return ctx.Profils.Include("Adresse.Arrondisement").AsNoTracking().Include("Image").Include("Preuve.Image").ToList();
            }
        }
    }
}