﻿using pfinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfinal.Manager
{
    public static class AdressesManager
    {
        public static Adresse Edit(Adresse adresse)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Adresse a = ctx.Adresses.FirstOrDefault(ad => ad.Id == adresse.Id);

                if(a != null)
                {
                    a.StreetName = adresse.StreetName;
                    a.ArrondisementId = adresse.ArrondisementId;
                    a.CivicNumber = adresse.CivicNumber;
                    a.CodePostal = adresse.CodePostal;
                    ctx.SaveChanges();
                    return a;
                }

                return null;
            }
        }

        public static Adresse Add(Adresse adresse)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Adresse adr = ctx.Adresses.Add(adresse);
                ctx.SaveChanges();
                return adr;
            }
        }

        public static List<Arrondisement> GetAllArrondissement()
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                return ctx.Arrondisement.ToList();
            }
        }

        public static Arrondisement GetArrondisementById(int id)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                return ctx.Arrondisement.FirstOrDefault(a => a.Id == id);
            }
        }

        public static Adresse GetAdresseById(int id)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                return ctx.Adresses.FirstOrDefault(a => a.Id == id);
            }
        }
    }
}