﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pfinal.Models;
using System.Web.Http.Cors;
using pfinal.Models.Form;

namespace pfinal.Manager
{
    public static class AnnoncesManager
    {
        public static List<Annonce> GetAll()
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                return ctx.Annonces.ToList();
            }
        }

        public static List<Annonce> GetAllByArrondissementId(int id)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                return ctx.Annonces.Include("Image").Include("TypeAnnonce").AsNoTracking().Where(a => a.ArrondisementId == id).ToList();
            }
        }

        public static List<Annonce> Search(SearchAnnonceResult sar)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                if (sar.TypeAnnonceId == 0)
                    return ctx.Annonces.Include("Image").Where(a => a.Title.Contains(sar.TitleContain) && a.ArrondisementId == sar.ArrondissementId).ToList();
                else if (sar.TypeAnnonceId > 0)
                    return ctx.Annonces.Include("Image").Where(a => a.Title.Contains(sar.TitleContain) && a.ArrondisementId == sar.ArrondissementId && a.TypeAnnonceId == sar.TypeAnnonceId).ToList();
                else
                    return null;
            }
        }

        public static Annonce Edit(Annonce annonce)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Annonce r = ctx.Annonces.FirstOrDefault(a => a.Id == annonce.Id);

                if (r != null)
                {
                    r.Title = annonce.Title;
                    r.TypeAnnonceId = annonce.TypeAnnonceId;
                    r.Description = annonce.Description;
                    r.ImageId = annonce.ImageId;
                    ctx.SaveChanges();
                    return r;
                }

                return null;
            }
        }


        public static bool Delete(int id)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Annonce annonce = null;
                annonce = ctx.Annonces.FirstOrDefault(a => a.Id == id);

                if (annonce != null)
                {
                    ctx.Annonces.Remove(annonce);
                    ctx.SaveChanges();
                    return true;
                }

                return false;
            }
        }

        public static Annonce Get(int id)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Annonce annonce = null;
                annonce = ctx.Annonces.Include("Image").FirstOrDefault(a => a.Id == id);
                return annonce;
            }
        }

        public static Annonce Add(Annonce annonce)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Annonce a = ctx.Annonces.Add(annonce);
                ctx.SaveChanges();
                return a;
            }
        }

        public static bool TypeAnnonceValid(int id)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                TypeAnnonce t = ctx.TypeAnnonces.FirstOrDefault(ti => ti.Id == id);

                if (t != null)
                    return true;
                else
                    return false;
            }
        }

        public static List<Annonce> GetAnnonceByUserId(String id)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                return ctx.Annonces.Include("Image").Where(a => a.ApplicationUserId == id).ToList();
            }
        }

        public static List<TypeAnnonce> GetAllType()
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                return ctx.TypeAnnonces.ToList();
            }
        }
    }
}