﻿using pfinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfinal.Manager
{
    public static class ImagesManager
    {
        public static Image Add(Image img)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Image image = ctx.Images.Add(img);
                ctx.SaveChanges();
                return image;
            }
        }
    }
}