﻿using pfinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfinal.Manager
{
    public static class PreuvesManager
    {
        public static Preuve Add(Preuve preuve)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Preuve p = ctx.Preuves.Add(preuve);
                ctx.SaveChanges();
                return p;
            }
        }

        public static void Delete(int id)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Preuve p = ctx.Preuves.FirstOrDefault(pr => pr.Id == id);
                
                if(p != null)
                {
                    ctx.Preuves.Remove(p);
                    ctx.SaveChanges();
                }
            }
        }

        public static Preuve Edit(Preuve preuve)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Preuve p = ctx.Preuves.FirstOrDefault(pr => pr.ApplicationUserId == preuve.ApplicationUserId);

                if (p != null)
                {
                    p.ImageId = preuve.ImageId;
                    ctx.SaveChanges();
                    return p;
                }
                return null;
            }
        }

        public static List<Preuve> GetAll()
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                return ctx.Preuves.ToList();
            }
        }

        public static Preuve GetPreuveByAppId(String appid)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                return ctx.Preuves.FirstOrDefault(p => p.ApplicationUserId == appid);
            }
        }
    }
}