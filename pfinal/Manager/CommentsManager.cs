﻿using pfinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace pfinal.Manager
{
    public class CommentsManager
    {
        public static List<Comment> GetAllsCommentByArrondissement(int id)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                return ctx.Comments.Where(c => c.ApplicationUser.Profil.Adresse.ArrondisementId == id).ToList();
            }
        }

        public static List<Comment> GetCommentsByApplicationUserId(String appId)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                return ctx.Comments.Where(c => c.ApplicationUserId == appId).ToList();
            }
        }

        public static Comment Add(Comment comment)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Comment c = ctx.Comments.Add(comment);
                ctx.SaveChanges();
                return c;
            }
        }

        public static void Delete(int id)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Comment comment = ctx.Comments.FirstOrDefault(c => c.Id == id);

                if (comment != null)
                {
                    ctx.Comments.Remove(comment);
                    ctx.SaveChanges();
                }
            }
        }

        public static Comment Edit(Comment comment)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                Comment co = ctx.Comments.FirstOrDefault(c => c.Id == comment.Id);

                if (co != null)
                {
                    co.Message = comment.Message;
                    ctx.SaveChanges();
                    return co;
                }

                return null;
            }
        }
    }
}