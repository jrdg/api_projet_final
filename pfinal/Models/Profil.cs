﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace pfinal.Models
{
    public class Profil
    {
        public String Firstname { get; set; }

        public String Lastname { get; set; }

        public String Email { get; set; }

        public String Gender { get; set; }

        public DateTime? Birthday { get; set; }

        public String Phone { get; set; }

        public bool? IsVerified { get; set;}

        [Key]
        [ForeignKey("ApplicationUser")]
        public String ApplicationUserId { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        public int? AdresseId { get; set; }

        public Adresse Adresse { get; set; }

        public int? ImageId { get; set; }
        public Image Image { get; set; }
        public int? PreuveId { get; set; }
        public Preuve Preuve { get; set; }
    }
}