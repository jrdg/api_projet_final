﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pfinal.Models.Form
{
    public class EditAdressesForm
    {
        [Required]
        public int ArrondisementId { get; set; }

        [Required]
        public String CivicNumber { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(6)]
        public String CodePostal { get; set; }

        [Required]
        public String StreetName { get; set; }
    }
}