﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pfinal.Models.Form
{
    public class EditAnnonceForm
    {
        [Required]
        public int AnnonceId { get; set; }
        [Required]
        public String Title { get; set; }
        [Required]
        public String Description { get; set; }
        [Required]
        public int TypeAnnonceId { get; set; }
    }

    public class NewAnnonceForm
    {
        [Required]
        public String Title { get; set; }
        [Required]
        public String Description { get; set; }
        [Required]
        public int TypeAnnonceId { get; set; }
    }
}