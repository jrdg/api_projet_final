﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfinal.Models.Form
{
    public class SearchAnnonceResult
    {
        public int TypeAnnonceId { get; set; }
        public int ArrondissementId { get; set; }
        public String TitleContain { get; set; }
    }
}