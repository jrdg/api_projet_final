﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pfinal.Models.Form
{
    public class EditProfilMain
    {
        [RegularExpression(pattern: "^([a-z]+[,.]?[ ]?|[a-z]+['-]?)+$", ErrorMessage = "Firstname is invalid")]
        public String Firstname { get; set; }

        [RegularExpression(pattern: "^([a-z]+[,.]?[ ]?|[a-z]+['-]?)+$", ErrorMessage = "Lastname is invalid")]
        public String Lastname { get; set; }

        public String Gender { get; set; }

        public String Year { get; set; }

        public String Month { get; set; }

        public String Day { get; set; }

        [RegularExpression(pattern: "[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]", ErrorMessage = "Phone number is invalid")]
        public String Phone { get; set; }
    }
}