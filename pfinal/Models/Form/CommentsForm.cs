﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pfinal.Models.Form
{
    public class NewCommentForm
    {
        [Required]
        public String Message { get; set; }
        public String ApplicationUserId { get; set; }
    }
}