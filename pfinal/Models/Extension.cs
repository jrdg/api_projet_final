﻿using pfinal.Models.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;

namespace pfinal.Models
{
    public static class Extension
    {

        public static bool IsInt(this String nb)
        {
            try
            {
                int nb2 = int.Parse(nb);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// On utilise cette methode pour renvoyer un array contenant le nom de chacun des attribut du model assosier a son erreur si il y en a.
        /// il faut lui donner le type de class qui es tester dans le modelstate exemple si le modelstate est une user on fais ModelState.getListErrorAndKey("le label" , typeof(User))
        /// </summary>
        /// <param name="ms"></param>
        /// <returns></returns>
        public static Error[] getListErrorAndKey(this ModelStateDictionary ms, String formLabelExtension, Type t)
        {
            List<Error> errorlist = new List<Error>();

            foreach (var item in ms)
            {
                Error err = new Error();

                if (item.Key != "key")
                {
                    if (item.Key.IndexOf('.') > 0)
                        err.Key = item.Key.Split('.')[1] + formLabelExtension;
                    else
                        err.Key = item.Key + formLabelExtension;

                    foreach (var item2 in item.Value.Errors)
                        err.Msg = item2.ErrorMessage;

                    errorlist.Add(err);
                }
            }


            foreach (var item in t.GetProperties())
            {
                bool put = true;

                foreach (var item2 in errorlist)
                {
                    if (item2.Key == item.Name + formLabelExtension)
                    {
                        put = false;
                        break;
                    }
                }

                if (put)
                    errorlist.Add(new Error { Key = item.Name + formLabelExtension, Msg = null });
            }

            return errorlist.ToArray();
        }
    }
}