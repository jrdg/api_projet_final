﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfinal.Models.NotMapped
{
    public class Error
    {
        public String Key { get; set; }
        public String Msg { get; set; }
    }
}