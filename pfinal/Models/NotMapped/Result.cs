﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace pfinal.Models.NotMapped
{
    public class Result
    {
        public HttpStatusCode HttpCode { get; set; }
        public Object Obj { get; set; }
    }
}