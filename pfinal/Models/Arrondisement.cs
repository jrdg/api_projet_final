﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfinal.Models
{
    public class Arrondisement
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Code { get; set; }
        public ICollection<Adresse> Adresses { get; set; }
    }
}