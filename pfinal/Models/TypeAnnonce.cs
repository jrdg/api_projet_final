﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfinal.Models
{
    public class TypeAnnonce
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public ICollection<Annonce> Annonces { get; set; }
    }
}