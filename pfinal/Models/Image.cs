﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfinal.Models
{
    public class Image
    {
        public int Id { get; set; }
        public String Path { get; set; }
        public String Name { get; set; }
        public String ContentType { get; set; }
    }
}