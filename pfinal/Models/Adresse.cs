﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace pfinal.Models
{
    public class Adresse
    {
        public int Id { get; set; }
        public String CivicNumber { get; set; }
        public String StreetName { get; set; }
        public String CodePostal { get; set; }
        public int ArrondisementId { get; set; }
        public Arrondisement Arrondisement { get; set; }
    }
}