﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfinal.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public String Message { get; set; }
        public DateTime DatePublication { get; set; }
        public String ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}