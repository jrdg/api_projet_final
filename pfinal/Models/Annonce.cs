﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace pfinal.Models
{
    public class Annonce
    {
        public int Id { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
        public DateTime DatePublication { get; set; }
        public int TypeAnnonceId { get; set; }
        public TypeAnnonce TypeAnnonce { get; set; }
        public String ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public int ArrondisementId { get; set; }
        public Arrondisement Arrondisement { get; set; }
        public int? ImageId { get; set; }
        public Image Image { get; set; }
    }
}