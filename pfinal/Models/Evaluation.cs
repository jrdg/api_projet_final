﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pfinal.Models
{
    public class Evaluation
    {
        public int Id { get; set; }
        public int Score { get; set; }
        public String Message { get; set; }
        public String ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}